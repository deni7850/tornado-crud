import os

DATABASE_USER = os.getenv("DATABASE_USER", default='postgres')
DATABASE_PASSWORD = os.getenv("DATABASE_PASSWORD", 'password')
DATABASE_HOST = os.getenv("DATABASE_HOST", default="localhost")
DATABASE_PORT = os.getenv("DATABASE_PORT", default="5432")
DATABASE_NAME = os.getenv("DATABASE_NAME", default='request_statistics')

APP_PORT = int(os.getenv('APP_PORT', default=8888))

DATABASE_URL = os.environ.get(
    'DATABASE_URL',
    default=f"postgresql://{DATABASE_USER}:{DATABASE_PASSWORD}@{DATABASE_HOST}:{DATABASE_PORT}/{DATABASE_NAME}")
