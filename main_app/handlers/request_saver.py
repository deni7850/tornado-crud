import json

from tornado.web import RequestHandler
from tornado_sqlalchemy import SessionMixin

from main_app.services.services import RequestService
import main_app.schemas.request as schemas


class RequestSaverHandler(RequestHandler, SessionMixin):
    # в fastapi можно сделать так, для того, чтобы у router уже был префикс
    #   и можно указать к POST методу путь в декораторе:
    # router = APIRouter(prefix='/api', tags=['city'])
    # @router.post('/add', summary='Save Request', description='Сохранение запроса')
    #   как сделать тоже самое в торнадо я не нашел, ну и не понял, насколько применимо тут
    #   https://www.tornadoweb.org/en/stable/routing.html
    #   кажется, что стоит объединить post get(request по ключу) delete put в один router
    #   и включить(include) его в app
    async def post(self):
        data = json.loads(self.request.body)
        income_model = schemas.IncomeRequest(body_content=data)
        with self.make_session() as session:
            saved_request = await RequestService(session).create(income_model)
        self.write({'key': saved_request.key})


class RequestGetterHandler(RequestHandler, SessionMixin):
    async def get(self):
        param = schemas.GetByKeyRequest.parse_obj(self.request.arguments)
        with self.make_session() as session:
            request = await RequestService(session).get_by_key(param)
        self.write(request.dict())


class RequestRemovalHandler(RequestHandler, SessionMixin):
    async def delete(self, ):
        param = schemas.GetByKeyRequest.parse_obj(self.request.arguments)
        with self.make_session() as session:
            await RequestService(session).remove_by_key(param)


class RequestUpdateHandler(RequestHandler, SessionMixin):
    async def put(self, ):
        # Изменить тело запроса и вернуть новый ключ, обнулить счетчик дубликатов
        #  - счетчик дубликатов обновится, ввиду того, что это запрос с новым ключом,
        #  нужно ли удалять старые записи?
        data = json.loads(self.request.body)
        param = schemas.GetByKeyRequest.parse_obj(self.request.arguments)
        param = schemas.UpdateByKeyRequest(
            body_content=data,
            key=param.key
        )
        with self.make_session() as session:
            saved_request = await RequestService(session).update_by_key(param)
        self.write({'key': saved_request.key})
