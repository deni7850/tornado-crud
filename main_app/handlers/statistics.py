from tornado.web import RequestHandler
from tornado_sqlalchemy import SessionMixin

from main_app.services.statistics import StatisticsService


class RequeststatisticHandler(RequestHandler, SessionMixin):
    async def get(self, ):
        with self.make_session() as session:
            saved_request = await StatisticsService(session).get_statistics()
        self.write({"duplicates": f"{saved_request.duplicate_percent_of_requests * 100}%"})
