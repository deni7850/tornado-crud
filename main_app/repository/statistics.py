from sqlalchemy import func

from main_app import models
from main_app.repository import AppRepository
import main_app.schemas.statistics as schemas


class StatisticsRepository(AppRepository):
    async def get_percent_of_duplicates(self) -> schemas.StatisticsResponse:
        grouped_request = self.session \
            .query(models.SavedRequest.key, func.count(models.SavedRequest.key)) \
            .group_by(models.SavedRequest.key)
        duplicates = grouped_request.having(func.count(models.SavedRequest.key) > 1).count()
        all_requests = grouped_request.count()
        try:
            return schemas.StatisticsResponse(duplicate_percent_of_requests=duplicates / all_requests)
        except ZeroDivisionError:
            return schemas.StatisticsResponse(duplicate_percent_of_requests=0)
