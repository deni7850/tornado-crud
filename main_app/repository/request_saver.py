import json

from sqlalchemy import func
from sqlalchemy.orm import Session

import main_app.models as models
import main_app.schemas.request as schemas
from main_app.repository import AppRepository


class RequestSaverRepository(AppRepository):
    async def create(self, request: schemas.BaseRequestStructure) -> schemas.SavedRequest:
        """Сохранение запроса в БД"""
        request_object = models.SavedRequest(**request.dict())
        self.session.add(request_object)
        self.session.commit()
        result = schemas.SavedRequest.from_orm(request_object)
        return result

    async def get_request_by_key(self, key: str) -> schemas.GetByKeyResponse:
        """Поиск запроса по ключу и группировка по телу для подсчета дубликатов"""
        request = self.session \
            .query(models.SavedRequest.body_content, func.count(models.SavedRequest.body_content)) \
            .where(models.SavedRequest.key == key) \
            .group_by(models.SavedRequest.body_content) \
            .first()
        # нужно ли считать дубликатом первый запрос? если да, то счетчик дубликатов можно уменьшить на 1
        result = schemas.GetByKeyResponse(
            duplicates=request[1],
            body_content=json.loads(request[0])
        )
        return result

    async def remove_by_key(self, key: str) -> None:
        """Удаление всех запросов по ключу"""
        self.session.query(models.SavedRequest).filter(models.SavedRequest.key == key).delete()
        self.session.commit()

    async def update_by_key(self, key: str, request: schemas.BaseRequestStructure) -> schemas.SavedRequest:
        """Обновление запроса в БД"""
        self.session: Session
        existing = self.session.query(models.SavedRequest).filter(models.SavedRequest.key == key).first()
        if existing:
            existing.key = request.key
            existing.body_content = request.body_content
            # Если нужно всё же удалять записи со старым ключом, раскомментировать строку ниже
            # self.session.query(models.SavedRequest).filter(models.SavedRequest.key == key).delete()
            self.session.commit()
        else:
            raise ValueError(f'No entries for key {key}')

        result = schemas.SavedRequest.from_orm(existing)
        return result
