import base64
from typing import Union, List

from pydantic import BaseModel, validator


def check_base64(v):
    """Проверяет, декодируется ли base64-содержимое."""
    try:
        base64.b64decode(v)
    except Exception:
        raise TypeError('Content must be base64 encoded')
    return v


class BaseRequestStructure(BaseModel):
    key: str
    body_content: str


class SavedRequest(BaseRequestStructure):
    id: int

    class Config:
        orm_mode = True


class IncomeRequest(BaseModel):
    """Структура входящего запроса"""
    body_content: dict


class GetByKeyRequest(BaseModel):
    key: List[Union[str, bytes]]

    @validator('key')
    def check_key(cls, value):
        if len(value) == 0:
            raise ValueError("Param key is required")
        else:
            check_base64(value[0])
        return value


class GetByKeyResponse(BaseModel):
    duplicates: int
    body_content: dict


class UpdateByKeyRequest(GetByKeyRequest):
    body_content: dict
