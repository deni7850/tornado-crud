from pydantic import BaseModel


class StatisticsResponse(BaseModel):
    duplicate_percent_of_requests: float
