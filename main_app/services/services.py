import base64
import json
from collections import OrderedDict

import main_app.schemas.request as schemas
from main_app.repository.request_saver import RequestSaverRepository
from main_app.services import AppService


class RequestService(AppService):
    @classmethod
    def _sort_value(cls, value):
        if isinstance(value, list):
            strs = list(filter(lambda x: type(x) == str, value))
            ints = list(filter(lambda x: type(x) == int, value))
            dicts = list(filter(lambda x: type(x) == dict, value))
            lists = list(filter(lambda x: type(x) == list, value))

            lists = [cls._sort_value(a) for a in lists]
            dicts = [cls._sort_value(a) for a in sorted(dicts, key=lambda d: list(d.keys()))]

            value = sorted(strs) + sorted(ints) + dicts + lists
        elif isinstance(value, dict):
            value = OrderedDict(sorted(value.items()))
            for k, v in value.items():
                value[k] = cls._sort_value(v)
        return value

    @classmethod
    def _sort_body(cls, body_content: dict):
        """Сортировка тела запроса и всех вложенных элементов"""
        # можно вынести в валидатор для класса IncomeRequest, так как он используется
        #   для создания/обновления модели
        body_content = cls._sort_value(body_content)
        return body_content

    @classmethod
    def _get_key(cls, params: dict):
        """Формирование ключа в соответсвии с требованиями задачи"""
        # Ключ генерируется из параметров тела запроса, методом “ключ+значение”, после
        # чего кодируется в base64:
        # тут непонятно, нужно ли вложенные поля учитывать,
        #  поэтому решил просто dump-ить все значения на первом уровне
        data_to_encode = []
        for k, v in params.items():
            if isinstance(v, (dict, list)):
                v = json.dumps(v, sort_keys=True)
            data_to_encode.append('+'.join((k, str(v))))

        return base64.b64encode(''.join(data_to_encode).encode()).decode()

    async def create(self, request: schemas.IncomeRequest) -> schemas.SavedRequest:
        request.body_content = self._sort_body(request.body_content)
        request_to_save = schemas.BaseRequestStructure(
            key=self._get_key(request.body_content),
            body_content=json.dumps(request.body_content, sort_keys=True)
        )
        result = await RequestSaverRepository(self.session).create(request_to_save)
        return result

    async def get_by_key(self, request: schemas.GetByKeyRequest) -> schemas.GetByKeyResponse:
        result = await RequestSaverRepository(self.session).get_request_by_key(request.key[0])
        return result

    async def remove_by_key(self, request: schemas.GetByKeyRequest) -> None:
        return await RequestSaverRepository(self.session).remove_by_key(request.key[0])

    async def update_by_key(self, request: schemas.UpdateByKeyRequest) -> schemas.SavedRequest:
        request.body_content = self._sort_body(request.body_content)
        request_to_save = schemas.BaseRequestStructure(
            key=self._get_key(request.body_content),
            body_content=json.dumps(request.body_content, sort_keys=True)
        )
        return await RequestSaverRepository(self.session).update_by_key(request.key[0], request_to_save)
