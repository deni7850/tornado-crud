import main_app.schemas.statistics as schemas
from main_app.repository.statistics import StatisticsRepository
from main_app.services import AppService


class StatisticsService(AppService):
    async def get_statistics(self) -> schemas.StatisticsResponse:
        result = await StatisticsRepository(self.session).get_percent_of_duplicates()
        return result
