from sqlalchemy import Column, BigInteger, String
from tornado_sqlalchemy import SQLAlchemy

from main_app.conf import DATABASE_URL

db = SQLAlchemy(url=DATABASE_URL)


class SavedRequest(db.Model):
    """
    Сохраненный запрос
    """
    __tablename__ = 'saved_request'
    id = Column(BigInteger, primary_key=True)
    key = Column(String)
    body_content = Column(String)
