# tornado crud
Для запуска сервера
```
docker-compose -f docker-compose.yml up -d --build
```
Для создания миграций:
```
alembic revision --autogenerate -m "Added table saved requests"
```
Выполнение миграций
```
alembic upgrade head
```
