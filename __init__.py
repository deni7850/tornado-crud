from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.options import define, options
from tornado.web import Application
from tornado_sqlalchemy import SQLAlchemy

import main_app.handlers.request_saver as request_handlers
import main_app.handlers.statistics
from main_app.conf import DATABASE_URL, APP_PORT

define('port', default=APP_PORT, help='port to listen on')


def get_db():
    return SQLAlchemy(DATABASE_URL)
    # try:
    #     yield db
    # finally:
    #     db.close()


def main():
    """Construct and serve the tornado application."""
    app = Application([
        ('/api/add', request_handlers.RequestSaverHandler),
        ('/api/get', request_handlers.RequestGetterHandler),
        ('/api/remove', request_handlers.RequestRemovalHandler),
        ('/api/update', request_handlers.RequestUpdateHandler),
        ('/api/statistic', main_app.handlers.statistics.RequeststatisticHandler),
    ],
        db=get_db()
    )
    http_server = HTTPServer(app)
    http_server.listen(options.port)
    print('Listening on http://localhost:%i' % options.port)
    IOLoop.current().start()


if __name__ == '__main__':
    main()
